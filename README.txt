WebINK module

SUMMARY

This module lets you use web fonts from WebINK (http://www.webink.com).

PREREQUISITES

WebINK depends on @font-your-face (http://drupal.org/project/fontyourface).
It also requires PHP SoapClient library to connect to the WebINK server.

MISCELLANOUS INFORMATION

-- PROJECTS (a.k.a. 'TYPEDRAWERS')

Fonts are imported only from those projects that are detected to match current
domain.

-- DUPLICATE FONTS

WebINK allows you to add the same font to several projects; this module would
however import it only once.

ROADMAP

- Look into whether fonts in our database but not found in API anymore should
  be removed. Other submodules do not do this, however.

- Look into parsing more information about projects. It does not really fit
  anywhere in the browsing interface, but we could display a small overview
  table on the settings page, with project names, font counts and domains, and
  probably in a collapsible fieldset.

- Look into possibilities of more aggressive synchronizing - e.g. check whether
  API results are up-to-date before enabling a font.
